/*
    Folder structure:
        ├── problem1.cjs
        ├── problem2.cjs
        └── test
            ├── testProblem1.cjs
            └── testProblem2.cjs
*/

/*
    Problem 1:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Create a directory of random JSON files
        2. Delete those files simultaneously 
*/

/*


    Problem 2:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/

const fs = require("fs");
const path = require("path");

/* let pathFIle = path.join(__dirname, "/random JSON");
console.log(pathFIle);
 */
function makeDirectory(directoryName, numberOfFiles, createFilecallback) {
  fs.mkdir(`${directoryName}`, (err) => {
    if (err) {
      return;
    } else {
      console.log("folder is created");
      if (numberOfFiles) {
        createFilecallback(directoryName, numberOfFiles, deleteFiles);
      }
    }
  });
}

function createFiles(directoryPath, numberOfFiles, deleteFileCallBack) {
  let loop = 0;
  let file = 0;
  while (loop < numberOfFiles) {
    fs.writeFile(
      `${directoryPath}/file${loop + 1}`,
      `file ${loop + 1} is created`,
      (err) => {
        if (err) {
          console.log("error occured: ", err);
          return;
        }
        file++;

        if (file == numberOfFiles) {
          deleteFileCallBack(directoryPath, numberOfFiles);
        }
      }
    );
    loop++;
    console.log(`file ${loop} created`);
  }
}



function deleteFiles(directoryPath, numberOfFiles) {
  let loop;
  for (loop = 0; loop < numberOfFiles; loop++) {
    fs.unlink(path.join(directoryPath, `file${loop + 1}`), (err) => {
      if (err) {
        console.log("error happened");
        return;
      }
    });
  }
  if (loop == numberOfFiles) {
    fs.rmdir(directoryPath, (err) => {
      if (err) {
        console.log(err);
      }
      console.log("all files and folder is deleted");
    });
  }
}

function fsProblem1(directoryPath, numberOfFiles) {
  makeDirectory(directoryPath, numberOfFiles, createFiles);
}



module.exports = fsProblem1